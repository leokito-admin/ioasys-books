# Desafio ioasys

## Instalação

clonar o repositório com

### `git clone`

Para iniciar, rodar na raíz do projeto:

### `yarn`

Para rodar a applicação no modo desevolvedor, primeiro utilizar:

### `yarn start`

Depois, abrir o navegador com o seguinte endereço: [http://localhost:3000](http://localhost:3000) to view it in your browser.

## Sobre o projeto

O seguinte desafio solicita a criação de uma landing page para login e um home que renderiza uma lista de livros a partir de uma API.

### Ferramentas utilizadas:

* Para desenvolver o projeto foi utilizado o styled-components.

### Principais Componentes:
* Provider auth: Para compartilhamento do dado de autenticação, token e refresh-token, foi criado um provider auth com ContextAPI.
* Component Header: Recebe o nome e o gênero do usuário para exibir uma saudação personalizada, possui o logo do ioasys Books e um botão para fazer o logout, ao clicar no botão logout o usuário é redirecionado à página Login e precisa realizar novamente o login para acessar a página Home.

* Component Card: Renderizado através de um map na página Home, exibe informações resumidas sobre o livro como: Titulo, Author, Número de Páginas, Editora e data de Publicação, clicando no card abre o modal que exibe detalhes.

* Componente Modal: Para exibir o detalhamento do livro, foi criado o component Modal, que recebe informações mais detalhadas do que as disponíveis no card. Para a exibição do modal, é necessário que o state isVisible esteja com o valor `true`, para fechar o modal, pode-se clicar tanto no x no canto superior direito, como nas áreas de fora do modal (área com blur).

## Integração com API

* A documentação da API está disponível a partir de uma página web (https://books.ioasys.com.br/api/docs/).

* Documentação: https://books.ioasys.com.br/api/docs/

* Servidor: https://books.ioasys.com.br/api/v1
Usuário de Teste: desafio@ioasys.com.br
Senha de Teste: 12341234

Desenvolvido por Leonardo Pereira