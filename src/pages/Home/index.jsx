import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import prevIcon from '../../assets/Images/icon-prev.svg'
import nextIcon from '../../assets/Images/icon-next.svg'
import { useAuth } from '../../providers/auth'
import Header from '../../components/Header'
import api from '../../services/api'
import { Container, Pagination, Wrapper } from './style'
import BookCard from '../../components/BookCard'

const amount = 12

const Home = () => {

  const {user, signOut, refreshToken} = useAuth();

  const [books, setBooks] = useState([])
  const [page, setPage] = useState(1)
  const [pagesTotal, setPagesTotal] = useState(1)

  const navigate = useNavigate()

useEffect(()=> {
  async function getBooks() {
    try {
      
      const response = await api
      .get(`/books?page=${page}&amount=${amount}`)
      setBooks(response.data.data)
      setPagesTotal(Math.ceil(response.data.totalPages))
    } catch (error) {
      signOut()
      navigate('/sign-in')
    }
  }

  getBooks()
  }, [page])

  const prevPage = () => {
    if (page - 1 > 0) {
      setPage(oldValue => oldValue - 1)
    }
  }

  const nextPage = () => {
    if (page + 1 < pagesTotal) {
      setPage(oldValue => oldValue + 1)
    }
  }

  return (
<>
    <Container>
      <div>
      <Header user={user}/>
    </div>
    <Wrapper>
      <ul>
        {books && books.map((book, index) => {
          return(
            <>
            <BookCard key={index} book={book} />
            </>
          )
        })}
      </ul>
      <Pagination>
            <span>
              Página <strong>{page}</strong> de <strong>{pagesTotal}</strong>
            </span>

            <button
              type="button"
              onClick={prevPage}
              disabled={!books || page === 1}
            >
              <img src={prevIcon} alt="Anterior" />
            </button>

            <button
              type="button"
              onClick={nextPage}
              disabled={!books || page === pagesTotal}
            >
              <img src={nextIcon} alt="Proximo" />
            </button>
          </Pagination>
      </Wrapper>
    </Container>
    </>
  )
}

export default Home