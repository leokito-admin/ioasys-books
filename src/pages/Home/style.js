import styled from 'styled-components';
import img from '../../assets/Images/bg-w.png'

export const Container = styled.div`
    width: 100%;
    height: 100vh;

    display: flex;
    flex-direction: column;
    align-items: center;

    background-image: url(${img});
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;

    padding-top: 48px;

    @media (max-width: 768px) {
      background-size: fill;
    }

    > div {
        width: 100%;
        max-width: 1368px;
        padding: 0 10px 0 0;
        margin: 0 auto;

      @media (max-width: 1366px) {
        max-width: 1168px;
    }

    @media (max-width: 1024px) {
      padding: 0 18px;
    }
  }
`
export const Wrapper = styled.section`

    margin-top: 80px;
    display: flex;
    flex-direction: column;
    align-items: flex-end;
    margin-right: 21px;
    max-width: 1368px;

    @media (max-width: 1366px) {
      max-width: 1168px;
      margin-top: 40px;
    }
    @media (max-width: 420px) {
      align-items: center;
    }
    @media (max-width: 1024px) {
      align-items: center;
      margin-left: 15px;
    }

    > ul {
      display: flex;
      gap: 16px;
      flex-wrap: wrap;
      gap: 16px;
    }
  
`
export const Pagination = styled.div`
  display: flex;
  padding: 16px 0;
  align-items: center;

  span {
    font-size: 12px;
    line-height: 20px;
    color: #333333;
    margin-right: 8px;

    strong {
      font-weight: 500;
    }

    @media (max-width: 420px) {
      order: 1;
      margin: 0 16px;
    }
  }

  button {
    width: 32px;
    height: 32px;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: transparent;
    border: 1px solid rgba(51, 51, 51, 0.2);
    border-radius: 50%;
    margin-left: 8px;

    &:disabled {
      pointer-events: none;

      img {
        opacity: 0.4;
      }
    }

    img {
      max-width: 16px;
    }

    @media (max-width: 420px) {
      &:first-child {
        order: 0;
        margin-right: 0;
      }

      &:last-child {
        order: 1;
      }
    }
  }
`