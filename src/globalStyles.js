import { createGlobalStyle } from 'styled-components'

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    border: none;
    list-style: none;
    text-decoration: none;
    outline: none;
    box-sizing: border-box;
    font-family: "Heebo", sans-serif;
  }

  html, body {
    overflow-x: hidden;
  }

  button {
    cursor: pointer;
  }
`
