import { Greeting, Container, Logo, LogoDiv, LogoText, Logout, UserDiv } from './style'
import img from '../../assets/Images/logo-ioasys-black.svg';
import logout from '../../assets/Images/icon-logout.svg'
import { useAuth } from '../../providers/auth';
import { useNavigate } from 'react-router-dom';

const Header = () => {

  const {user, signOut} = useAuth()

  const navigate = useNavigate()

  const greetingGender = () => {
    if (user?.gender === 'F') {
      return 'Bem-vinda'
    } else {
      return 'Bem-vindo'
    }
  }

  const handleSignOut = () => {
    navigate('/sign-in')
    signOut()
  }
  return (
    <Container>
        <LogoDiv>
            <Logo src={img} alt=''/>
            <LogoText>Books</LogoText>
        </LogoDiv>
        <UserDiv>
            <Greeting>{greetingGender()}, <strong>{user?.name}</strong>!</Greeting>
            <Logout onClick={handleSignOut}>
            <img src={logout} alt='Sair'/>
            </Logout>
        </UserDiv>
    </Container>
  )
}

export default Header