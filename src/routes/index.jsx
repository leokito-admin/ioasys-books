import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom'

import Login from '../pages/Login'
import Home from '../pages/Home'

export default function Router() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Navigate to="/home" replace />
        } />
        <Route path="/home" element={
          <Home />
        } />
        <Route path="/sign-in" element={<Login />} />
      </Routes>
    </BrowserRouter>
  )
}
