import styled from 'styled-components';

export const Container = styled.header`
  max-width: 1368px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  background-color: transparent;

  @media (max-width: 1366px) {
    max-width: 1168px;
  }

  > div {
    max-width: 1368px;
    display: flex;
    align-items: center;
    gap: 16px;
    @media (max-width: 1366px) {
      max-width: 1168px;
    }

    h1 {
      font-weight: 300;
      font-size: 28px;
      line-height: 40px;
      color: #333333;
    }
   }
  }

`
export const LogoDiv = styled.div``
export const Logo = styled.img`
    height: 36px;
`

export const LogoText = styled.h1`

`
export const UserDiv = styled.div`

`
export const Greeting = styled.div`
    @media screen and (max-width: 767px) {
    display: none;
  }
`
export const Logout = styled.button`
      width: 32px;
      height: 32px;
      display: flex;
      align-items: center;
      justify-content: center;
      background-color: transparent;
      border: 1px solid rgba(51, 51, 51, 0.2);
      border-radius: 50%;

      img {
        max-width: 16px;
      }

      cursor: pointer;
`
export const LogoutIcon = styled.img``
