import styled from 'styled-components'

export const Container = styled.li`
  flex: auto;
  display: flex;
  align-items: center;
  gap: 16px;
  padding: 16px;
  background: #ffffff;
  box-shadow: 0px 6px 24px rgba(84, 16, 95, 0.13);
  border-radius: 4px;

  &:hover {
    box-shadow: 0 16px 80px 0 rgba(84, 16, 95, 0.3);
    cursor: pointer;
  }

  img {
    max-width: 95px;
    margin-right: 15px;
    @media screen and (max-width: 1366px) {
      max-width: 81px;
    }
  }

  div {
    width: 130px;
    @media screen and (max-width: 1366px) {
      width: 120px;
    }

    h3,
    span,
    p {
      display: -webkit-box;
      -webkit-line-clamp: 1;
      -webkit-box-orient: vertical;
      overflow: hidden;
    }

    h3 {

        font-weight: 800;
        font-size: 14px;
        line-height: 20px;
        color: #333333;
      
    }

    span {
      font-size: 12px;
      line-height: 20px;
      color: #ab2680;
      margin-bottom: 25px;
    }

    p {
      color: #999999;
      font-size: 12px;
      line-height: 20px;
      width: 110%;

      @media screen and (max-width: 1366px) {
        width: 100%;
      }
    }
  }
`
