import { createContext, useContext, useState } from "react";
import api from "../services/api";

const AuthContext = createContext()

export const AuthProvider = ({children}) => {
    const [data, setData] = useState(() => {
        const token = localStorage.getItem("@ioasys: token");
        const refreshToken = localStorage.getItem('@ioasys: refresh-token');
        const user = localStorage.getItem("@ioasys: user");

        if (token && user && refreshToken) {
            api.defaults.headers.common.Authorization = `Bearer ${token}`
            return {
                token, 
                refreshToken, 
                user: JSON.parse(user)}
        }
        return {token,
            refreshToken,
            user}
    });

    const signIn = async (loginData, navigate, setLoginError) => {
        await api
        .post('/auth/sign-in', loginData)
        .then((res) => {
            const data = res.data
            const token = res.headers.authorization
            const refreshToken = res.headers['refresh-token']
            
            localStorage.setItem('@ioasys: token', token)
            localStorage.setItem('@ioasys: refresh-token', refreshToken)
            localStorage.setItem('@ioasys: user', JSON.stringify(data))

            setData({
                token,
                refreshToken,
                user: data
            })

            navigate('/home')
        })
        .catch((e) => setLoginError(true))
    };

    const refreshToken = async () => {
        const oldToken = localStorage.getItem('ioasys@token')
        const oldRefreshToken = localStorage.getItem('ioasys@refresh-token')
    
        if (oldToken && oldRefreshToken) {
          const { headers } = await api.post('/auth/refresh-token', {
            refreshToken: oldRefreshToken
          })
    
          const token = headers.authorization
          const refreshToken = headers['refresh-token']
    
          localStorage.setItem('ioasys@token', token)
          localStorage.setItem('ioasys@refresh-token', refreshToken)
    
          api.defaults.headers.common.Authorization = `Bearer ${token}`
    
          setData(oldValue => ({
            token,
            refreshToken,
            user: oldValue.user
          }))
        }
      }

    const signOut = () => {
        localStorage.removeItem('@ioasys: token')
        localStorage.removeItem('@ioasys: refresh-token')
        localStorage.removeItem('@ioasys: user')

        setData({})
    };

    return(
        <AuthContext.Provider value={{user: data.user, signIn, signOut, refreshToken}}>
            {children}
        </AuthContext.Provider>
    );
};

export const useAuth = () => useContext(AuthContext)