import React, { useState } from 'react'
import { Container, ErrorBaloon, Form, FormHeader, Input, Label, Logo, SubmitInput, Title } from './style'
import logo from '../../assets/Images/logo-ioasys.svg'
import { useNavigate } from 'react-router-dom'
import arrow from '../../assets/Images/arrow.svg'
import { useForm } from 'react-hook-form'
import { useAuth } from '../../providers/auth'

const Login = () => {

  const navigate = useNavigate()

  const [loginError, setLoginError] = useState(false)

  const {signIn} = useAuth()

  const {
    register, 
    handleSubmit, 
    formState: { errors }, 
    setError } = useForm();

    const handleForm = (data) => {
      signIn(data, navigate, setLoginError)
  };

  return (
    <Container>
      <div>
        <Form onSubmit={handleSubmit(handleForm)}>
            <FormHeader>
                <Logo src={logo} alt='logo' />
                <Title>Books</Title>
            </FormHeader>
                <div className='form-content'>
                    <Label htmlFor='email'>
                        <div>
                        <span>Email</span>
                        <Input id='' 
                        autoComplete={'off'} 
                        {...register("email")}
                        />
                        </div>
                    </Label>
                    <Label htmlFor='password'>
                        <div>
                    <span>Senha</span>
                        <Input id='password' 
                        autoComplete={'off'} 
                        type={'password'}
                        {...register("password")}

                        />
                    </div>
                        <SubmitInput>Entrar</SubmitInput>
                    </Label>
                    <ErrorBaloon show={loginError}>
                      <img src={arrow} alt="Arrow" />
                      <p>Email e/ou senha incorretos.</p>
                    </ErrorBaloon>
                </div>
        </Form>
      </div>
    </Container>
  )
}

export default Login