import { useState } from 'react'
import Modal from '../Modal'
import defaultCover from '../../assets/Images/default.png'
import { Container } from './styles'

const BookCard = ({ book }) => {
  
  const [isVisible, setIsVisible] = useState(false)

  return (
    <>
      <Container onClick={() => setIsVisible(true)}>
        <img src={book?.imageUrl || defaultCover } alt="Capa do livro" />

        <div>
          <h3>{book.title}</h3>
          <span>{book.authors[0]}</span>

          <p>{book.pageCount} páginas</p>
          <p>Editora {book.publisher}</p>
          <p>Publicado em {book.published}</p>
        </div>
      </Container>
      {isVisible && (
        <Modal closeModal={() => setIsVisible(false)} book={book} />
      )}
    </>
  )
}

export default BookCard