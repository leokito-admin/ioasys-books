import closeIcon from '../../assets/Images/icon-close.svg'
import quoteImg from '../../assets/Images/quotes.svg'
import defaultCover from '../../assets/Images/default.png'

import { Background, Box, Content, List } from './style'

const Modal = ({ closeModal, book }) => {

  return (
    <Background onClick={closeModal}>
      <button type="button" onClick={closeModal}>
        <img src={closeIcon} alt="Fechar" />
      </button>

      <Box>
        <img src={book?.imageUrl || defaultCover} alt="Capa do livro" />

        <Content>
          <div>
            <h2>{book.title}</h2>
            <span>{book.authors.join(', ')}</span>
          </div>

          <div>
            <h3>Informações</h3>

            <List>
              <li>
                <strong>Páginas</strong>
                <p>{book.pageCount} páginas</p>
              </li>

              <li>
                <strong>Editora</strong>
                <p>{book.publisher}</p>
              </li>

              <li>
                <strong>Publicação</strong>
                <p>{book.published}</p>
              </li>

              <li>
                <strong>Idioma</strong>
                <p>{book.language}</p>
              </li>

              <li>
                <strong>Título Original</strong>
                <p>{book.title}</p>
              </li>

              <li>
                <strong>ISBN-10</strong>
                <p>{book.isbn10}</p>
              </li>

              <li>
                <strong>ISBN-13</strong>
                <p>{book.isbn13}</p>
              </li>
            </List>
          </div>

          <div>
            <h3>Resenha da editora</h3>

            <p>
              <img src={quoteImg} alt="Aspas" />
              {book.description}
            </p>
          </div>
        </Content>
      </Box>
    </Background>
  )
}

export default Modal