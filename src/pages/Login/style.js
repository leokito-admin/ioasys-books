import styled, {css} from 'styled-components'
import img from '../../assets/Images/bg.png'

export const Container = styled.div`
    width: 100%;
    height: 100vh;

    background-image: url(${img});
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
    
    display: flex;
    align-items: center;

    > div {
    width: 100%;
    max-width: 1168px;
    padding: 0 16px;
    margin: 0 auto;
  }
`
export const Form = styled.form`
    max-width: 368px;

    .form-header {
      display: flex;
      gap: 16px;

      margin-bottom: 48px;
    }

    .form-content {
        width: 100%;
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        gap: 16px;
    }
  `
export const FormHeader = styled.div`
  display: flex;
  gap: 16px;
  margin-bottom: 48px;
  
`
export const Logo = styled.img``
export const Title = styled.h1`
  font-weight: 300;
  font-size: 28px;
  line-height: 40px;
  color: #ffffff;
`
export const Label = styled.label`
    width: 100%;
    display: flex;
    
    align-items: center;
    gap: 8px;

    background: rgba(0, 0, 0, 0.32);
    backdrop-filter: blur(2px);
    border-radius: 4px;
    padding: 8px 16px;

    cursor: pointer;
    
    > div {
        width: 100%;
    };
    
    span {
        display: block;
        margin-bottom: 4px;
        font-size: 12px;
        line-height: 16px;
        color: #ffffff;
        opacity: 0.5;
    };
`
export const Input = styled.input`
    width: 100%;
    /* height: 38px; */
    background-color: transparent;
    border: 0;
    font-weight: normal;
    font-size: 16px;
    line-height: 24px;
    color: #ffffff;
    outline: none;
`
export const SubmitInput = styled.button`
    padding: 8px 20px;
    background: #ffffff;
    border-radius: 44px;
    border: none;
    font-weight: 500;
    font-size: 16px;
    line-height: 20px;
    color: #b22e6f;

    cursor:pointer;
`

export const InputDiv = styled.div`
    position: absolute;
    width: 368px;
    height: 60px;
    left: 115px;
    top: 360px;

    background: rgba(0, 0, 0, 0.32);
    backdrop-filter: blur(2px);
    
    border-radius: 4px;
`
export const ErrorBaloon = styled.div`
  opacity: 0;
  visibility: hidden;
  pointer-events: none;
  transform: translateY(-16px);
  transition: all 0.3s;

  ${props =>
    props.show &&
    css`
      opacity: 1;
      visibility: initial;
      pointer-events: all;
      transform: translateY(0);
    `}

  img {
    display: block;
    margin-left: 16px;
    margin-bottom:-16px;
  }

  p {
    padding: 16px;
    background: rgba(255, 255, 255, 0.4);
    backdrop-filter: blur(2px);
    border-radius: 4px;
    font-weight: bold;
    font-size: 16px;
    line-height: 16px;
    color: #ffffff;
  }
`