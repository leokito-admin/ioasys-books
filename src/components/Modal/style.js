import styled from 'styled-components'

export const Background = styled.div`
  width: 100%;
  height: 100%;
  position: fixed;
  left: 0;
  right: 0;
  top: 0;
  overflow-y: auto;
  z-index: 1;

  display: flex;
  align-items: center;
  justify-content: center;
  padding: 64px 16px;

  background: rgba(0, 0, 0, 0.4);
  backdrop-filter: blur(2px);

  @media (max-width: 680px) {
    display: block;
  }

  > button {
    position: absolute;
    top: 16px;
    right: 16px;
    width: 32px;
    height: 32px;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: #ffffff;
    border: 1px solid rgba(51, 51, 51, 0.2);
    border-radius: 50%;

    img {
      max-width: 16px;
    }
  }
`

export const Box = styled.div`
  padding: 48px;
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 48px;

  background-color: #ffffff;
  box-shadow: 0px 16px 80px rgba(0, 0, 0, 0.32);
  border-radius: 4px;

  img {
    max-width: 349px;
  }

  @media (max-width: 680px) {
    flex-direction: column;
    justify-content: flex-start;
    padding: 24px;
    gap: 24px;

    img {
      max-width: 100%;
    }
  }
`

export const Content = styled.div`
  max-width: 276px;
  display: flex;
  flex-direction: column;
  gap: 32px;

  @media (max-width: 680px) {
    max-width: 100%;
  }

  h2 {
    font-weight: 500;
    font-size: 28px;
    line-height: 40px;
    color: #333333;

    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
  }

  span {
    font-size: 12px;
    line-height: 20px;
    color: #ab2680;
  }

  h3 {
    font-weight: 500;
    font-size: 12px;
    line-height: 28px;
    text-transform: uppercase;
    color: #333333;
    margin-bottom: 8px;
  }

  p {
    font-size: 12px;
    line-height: 20px;
    color: #999999;

    img {
      margin-right: 4px;
    }
  }
`

export const List = styled.ul`
  li {
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;

    strong {
      font-weight: 500;
      font-size: 12px;
      line-height: 20px;
      color: #333333;
    }
  }
`
